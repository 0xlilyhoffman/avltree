# AVL Tree

For this project, I implemented an AVL Tree data structure in Java and wrote unit tests for it using the TestNG library. 

An AVL tree is a self-balancing Binary Search Tree where the difference between heights of left and right subtrees cannot be more than one for all nodes. If any modification of the tree results in a violation of the AVL condition, the tree must be rebalanced. The benefit of this structure is that the operations search, insert, and delete can all be performed in O(logn) time. 