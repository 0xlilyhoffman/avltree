import java.lang.Math;

/******
 * Lily Hoffman
 * COMP285 Data Structures & Algorithms
 *
 * AVLTreeNode.java
 *
 * This class implements AVL Tree data structure that stores integer values.
 * An AVL tree is a balanced binary search tree. At every node, the heights of the left and right subtrees can differ by at most 1
 * If an insertion results in a violation of the AVL condition, the tree must be rebalanced.
 * This implementation uses lazy deletion, so deleting items never results in a violation of the AVL condition
 *
 *
 ******/


public class AVLTreeNode{

    //Tree properties
    private AVLTreeNode left;
    private AVLTreeNode right;
    private int value;

    //Flag for lazy deletion
    private boolean deleted;


    /**
     * Public getter for left subtree
     *
     * @return AVLTreeNode for left subtree
     */
    public AVLTreeNode getLeft() {
        return this.left;
    }

    /**
     * Public getter for right subtree
     *
     * @return AVLTreeNode for right subtree
     */
    public AVLTreeNode getRight() {
        return this.right;
    }

    /**
     * Public getter for value stored in this node
     *
     * @return value stored in this node
     */
    public int getValue() {
        return this.value;
    }

    /**
     * Constructor to initialize an AVLTreeNode with a given integer value and no children.
     * @param value an integer value to store in the newly created AVLTreeNode
     */
    public AVLTreeNode(int value) {
        this.value = value;
        this.left = null;
        this.right = null;
        this.deleted = false;
    }

    /**
     * Computes height of tree
     * @param t root of tree
     * @return height of tree
     */
    private int getHeight(AVLTreeNode t){
        if(t == null)
            return 0;
        else
            return 1 + Math.max(getHeight(t.left), getHeight(t.right));
    }

    /**
     * Computes height difference between left and right subtrees
     * @param t root
     * @return height difference between left and right subtrees
     */
    private int heightDifference(AVLTreeNode t){
        int difference = getHeight(t.right)-getHeight(t.left);
        return Math.abs(difference);
    }


    /**
     * Inserts a new node with value into the AVL Tree, and re-balances the tree if necessary. Does nothing
     * if a node containing value is already present in the tree.
     *
     * @param value an integer value to insert
     *
     * @return the root of the tree after the insertion (in case the re-balancing altered the root node)
     */

    public AVLTreeNode insert(int value){
        if(value == this.value){
            if(this.deleted)
                this.deleted = false;
            return this;
        }
        else if (this.right == null && this.left == null){
            if(value > this.value)
                this.right = new AVLTreeNode(value);
            else
                this.left = new AVLTreeNode(value);
        }

        else if(this.right == null){
            if(value > this.value)
                this.right = new AVLTreeNode(value);
            else
                this.left = (this.left).insert(value);
        }

        else if(this.left == null){
            if(value < this.value)
                this.left = new AVLTreeNode(value);
            else
                this.right = (this.right).insert(value);
        }

        else{
            if(value > this.value)
                this.right = (this.right).insert(value);
            else
                this.left = (this.left).insert(value);
        }

        if( heightDifference(this) <= 1)//No re-balancing necessary
            return this;
        else{
            AVLTreeNode x =  rebalanceTree(this);
            return x;
        }
    }

    /**
     * Rebalances tree to preserve AVL balance condition
     * if an insertion caused the height of the right and left subtree
     * to differ by more than 1
     *
     * @param t root of subtree that caused AVL violation
     * @return new root of subtree
     */
    private AVLTreeNode rebalanceTree(AVLTreeNode t){

        if(getHeight(t.left) > getHeight(t.right)){
            if(getHeight(t.left.left)>=getHeight(t.left.right))
                t = rotateLeft(t);
            else
                t = rotateDoubleLeft(t);
        }
        else if(getHeight(t.right) >=getHeight(t.left)){
            if(getHeight(t.right.right) > getHeight(t.right.left))
                t = rotateRight(t);
            else
                t = rotateDoubleRight(t);
        }
        return t;
    }

    /**
     * Insertion into the left most node of the left subtree
     * caused an imbalance
     *
     * @param k2 root of deepest AVL violation
     * @return new root after rotation/rebalance
     */
    private AVLTreeNode rotateLeft(AVLTreeNode k2){
        AVLTreeNode k1 = k2.left;
        AVLTreeNode Y = k1.right;
        k1.right = k2;
        k2.left = Y;
        return k1;
    }


    /**
     * Insertion into the right most node of the right subtree
     * caused an imbalance
     *
     * @param k2 root of deepest AVL violation
     * @return new root after rotation/rebalance
     */
    private AVLTreeNode rotateRight(AVLTreeNode k2){
        AVLTreeNode k1 = k2.right;
        AVLTreeNode Y = k1.left;
        k1.left = k2;
        k2.right = Y;
        return k1;
    }


    /**
     * Insertion into the right subtree of the left child
     * caused an imbalance
     *
     * @param k3 root of deepest AVL violation
     * @return new root after rotation/rebalance
     */
    private AVLTreeNode rotateDoubleLeft(AVLTreeNode k3){
        AVLTreeNode k2 = k3.left;
        AVLTreeNode k1 = k2.right;
        AVLTreeNode B = k1.left;
        AVLTreeNode C = k1.right;
        k1.right = k3;
        k1.left = k2;
        k2.right = B;
        k3.left = C;
        return k1;
    }


    /**
     * Insertion into the left subtree of the right child
     * caused an imbalance
     *
     * @param k3 root of deepest AVL violation
     * @return new root after rotation/rebalance
     */
    private AVLTreeNode rotateDoubleRight(AVLTreeNode k3){
        AVLTreeNode k2 = k3.right;
        AVLTreeNode k1 =k2.left;
        AVLTreeNode A = k3.left;
        AVLTreeNode B = k1.left;
        AVLTreeNode C = k1.right;
        AVLTreeNode D = k2.right;
        k1.left = k3;
        k1.right = k2;
        k3.right = B;
        k2.left = C;
        return k1;
    }


    /**
     * Checks to see if a node containing value is present in the AVL Tree
     */
    public boolean contains(int value){
        if(this.find(value) != null)
            return true;
        else
            return false;
    }

    /**
     * Finds a node containing "value"
     * @param value - data to search for in tree
     * @return Node containing "value" if found;
     *         null if "value" is not found in tree
     */
    private AVLTreeNode find(int value){
        if(value == this.value){
            if(deleted)
                return null;
            return this;
        }
        else if (value < this.value){
            if(left != null)
                return left.find(value);
            else
                return null;
        }
        else{
            if (right != null)
                return right.find(value);
            else
                return null;
        }
    }

    /**
     * Searches for a node containing value and deletes it if present. Performs lazy deletion in order to preserve
     * AVL condition
     *
     * @param value an integer value to delete
     * @return true if a node was deleted, false if no node containing value was present
     */
    public boolean delete(int value){
        AVLTreeNode node = this.find(value);
        if(node != null){
            node.deleted = true;
            return true;
        }
        return false;
    }
}
