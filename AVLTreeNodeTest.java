import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.internal.PackageUtils;
//import org.testng.annotations.BeforeMethod;


public class AVLTreeNodeTest {



    @Test
    public void deleteNull() throws Exception
    {
        AVLTreeNode root = new AVLTreeNode(10);
        root.delete(30);
        Assert.assertFalse(root.delete(30));

    }

    @Test
    public void testContainsSimple() throws Exception
    {
        AVLTreeNode root = new AVLTreeNode(10);
        root = root.insert(1);
        root = root.insert(100);
        root = root.insert(1000);

        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1));
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(1000));
    }

    @Test
    public void testGetValue() throws Exception
    {
        AVLTreeNode root = new AVLTreeNode(10);
        Assert.assertTrue(root.getValue() == 10);
    }


    @Test
    public void testGetRight() throws Exception
    {
        AVLTreeNode root = new AVLTreeNode(10);
        root = root.insert(1);
        root = root.insert(100);
        root = root.insert(1000);
        Assert.assertTrue(root.getRight().getValue() == 100);
        Assert.assertTrue(root.getRight().getRight().getValue() == 1000);
    }

    @Test
    public void testGetLeft() throws Exception
    {
        AVLTreeNode root = new AVLTreeNode(100);
        root = root.insert(10);
        root = root.insert(1000);
        root = root.insert(1);

        Assert.assertTrue(root.getLeft().getValue() == 10);
        Assert.assertTrue(root.getLeft().getLeft().getValue() == 1);
    }


    @Test
    public void testRightRotationRoot() throws Exception
    {
        /*Build tree with no violations*/
        AVLTreeNode root = new AVLTreeNode(1);
        root = root.insert(2);

        /*Check tree structure*/
        Assert.assertTrue(root.contains(1));
        Assert.assertTrue(root.contains(2));

        /*Insert violation - trigger Right rotation*/
        root = root.insert(3);

        /*Test tree structure*/
        Assert.assertTrue(root.contains(1));
        Assert.assertTrue(root.contains(2));
        Assert.assertTrue(root.contains(3));



    }
    @Test
    public void testLeftRotationRoot() throws Exception
    {
        /*Build Tree*/
        AVLTreeNode root = new AVLTreeNode(3);
        root = root.insert(2);

        /*Check tree structure*/
        Assert.assertTrue(root.contains(3));
        Assert.assertTrue(root.contains(2));

        /*Add node to trigger left rotation at root*/
        root = root.insert(1);

        /*Check tree structure*/
        Assert.assertTrue(root.contains(3));
        Assert.assertTrue(root.contains(2));
        Assert.assertTrue(root.contains(1));

    }
    @Test
    public void testDoubleRightRotationRoot() throws Exception
    {
        /*Build Tree with no violations*/
        AVLTreeNode root = new AVLTreeNode(10);
        root = root.insert(20);

        /*Check tree structure*/
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(20));

        /*Insert node to trigger doubleRotation*/
        root = root.insert(15);

        /*Check tree structure*/
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(20));
        Assert.assertTrue(root.contains(15));
    }

    @Test
    public void testDoubleLeftRotationRoot() throws Exception
    {
        /*Build Tree with no violations*/
        AVLTreeNode root = new AVLTreeNode(20);
        root = root.insert(10);

        /*Check tree structure*/
        Assert.assertTrue(root.contains(20));
        Assert.assertTrue(root.contains(10));

        /*Insert node to trigger doubleRotation*/
        root = root.insert(15);

        /*Check tree structure*/
        Assert.assertTrue(root.contains(20));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(15));

    }

    @Test
    public void testRightRotationNONRoot() throws Exception
    {
        /*Build tree no violations*/
        AVLTreeNode root = new AVLTreeNode(100);
        root = root.insert(10);
        root = root.insert(1000);
        root = root.insert(2000);

        /*Check structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(2000));

        /*Insert node to tigger right rotation at node (1k)*/
        root = root.insert(3000);

        /*Check structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(2000));
        Assert.assertTrue(root.contains(3000));
    }

    @Test
    public void testLeftRotationNONRoot() throws Exception
    {
        /*Build tree no violations*/
        AVLTreeNode root = new AVLTreeNode(100);
        root = root.insert(10);
        root = root.insert(1000);
        root = root.insert(5);

        /*Check tree structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(5));

        /*Trigger Left rotation*/
        root = root.insert(1);

        /*Check tree structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(5));
        Assert.assertTrue(root.contains(1));
    }

    @Test
    public void testDoubleRightRotationNONRoot() throws Exception
    {
        /*Build tree no violations*/
        AVLTreeNode root = new AVLTreeNode(100);
        root = root.insert(10);
        root = root.insert(1000);
        root = root.insert(2000);

        /*Check structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(2000));

        /*Insert node to tigger right rotation at node (1k)*/
        root = root.insert(1500);

        /*Check structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(2000));
        Assert.assertTrue(root.contains(1500));
    }

    @Test
    public void testDoubleLeftRotationNONRoot() throws Exception
    {
        /*Build tree no violations*/
        AVLTreeNode root = new AVLTreeNode(100);
        root = root.insert(10);
        root = root.insert(1000);
        root = root.insert(5);

        /*Check tree structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(5));

        /*Trigger Left rotation*/
        root = root.insert(7);

        /*Check tree structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(5));
        Assert.assertTrue(root.contains(7));
    }


    @Test
    public void testMassiveRightRotations() throws Exception
    {
        AVLTreeNode root = new AVLTreeNode(0);

        for(int i = 1; i < 20; i++)
            root = root.insert(i);

        for(int i = 1; i<20; i++)
            Assert.assertTrue(root.contains(i));
    }

    @Test
    public void testMassiveLeftRotations() throws Exception
    {
        AVLTreeNode root = new AVLTreeNode(20);

        for(int i = 19; i < 0; i--)
            root = root.insert(i);

        for(int i = 19; i < 0; i--)
            Assert.assertTrue(root.contains(i));
    }

    @Test
    public void testSporadicDeletions ()throws Exception
    {
        AVLTreeNode root = new AVLTreeNode(0);

        //build
        for(int i = 1; i < 20; i++)
            root = root.insert(i);

        //check
        for(int i = 1; i < 20; i++)
            Assert.assertTrue(root.contains(i));

        //delete half of nodes
        for(int i = 1; i < 20; i+= 2)
            root.delete(i);

        //check desired nodes deleted
        for(int i = 1; i < 20; i+= 2)
            Assert.assertFalse(root.contains(i));

        //check desired nodes still present
        for(int i = 2; i<20; i+= 2)
            Assert.assertTrue(root.contains(i));

        //re-insert
        for(int i = 1; i < 20; i+= 2)
            root.insert(i);

        //check all
        for(int i = 1; i < 20; i++)
            Assert.assertTrue(root.contains(i));

        //re-delete
        for(int i = 1; i < 20; i+= 2)
            root.delete(i);

        //re-check desired nodes deleted
        for(int i = 1; i < 20; i+= 2)
            Assert.assertFalse(root.contains(i));

        //re-check desired nodes still present
        for(int i = 2; i<20; i+= 2)
            Assert.assertTrue(root.contains(i));


    }

    @Test
    public void testGetDeletedValue() throws Exception
    {
        /*Should I be allowed to "getValue" of deleted node though...*/
        AVLTreeNode root = new AVLTreeNode(100);
        root = root.insert(10);
        root = root.insert(1000);

        Assert.assertTrue(root.getLeft().getValue() == 10);
        Assert.assertTrue(root.contains(10));
        root.delete(10);
        Assert.assertFalse(root.contains(10));
        Assert.assertTrue(root.getLeft().getValue() == 10);
    }

    @Test
    public void testDeleteAndReinsertRoot() throws Exception
    {
        /*Build Tree*/
        AVLTreeNode root = new AVLTreeNode(100);
        root = root.insert(10);
        root = root.insert(1000);
        root = root.insert(1);

        /*Test structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(1));

        /*Delete Root*/
        root.delete(100);

        /*Test Structure*/
        Assert.assertFalse(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(1));

        /*Re-insert root*/
        root.insert(100);

        /*Test structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(1));

    }

    @Test
    public void testDeleteAndReinsertNode() throws Exception
    {
        /*Build Tree*/
        AVLTreeNode root = new AVLTreeNode(100);
        root = root.insert(10);
        root = root.insert(1000);
        root = root.insert(1);

        /*Test structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(1));

        /*Delete Node*/
        root.delete(10);

        /*Test Structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertFalse(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(1));

        /*Re-insert node*/
        root.insert(10);

        /*Test structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(1));
    }

    @Test
    public void testDeleteAndReinsertLeaf() throws Exception
    {
        /*Build Tree*/
        AVLTreeNode root = new AVLTreeNode(100);
        root = root.insert(10);
        root = root.insert(1000);
        root = root.insert(1);

        /*Test structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(1));

        /*Delete Root*/
        root.delete(1000);

        /*Test Structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertFalse(root.contains(1000));
        Assert.assertTrue(root.contains(1));

        /*Re-insert root*/
        root.insert(1000);

        /*Test structure*/
        Assert.assertTrue(root.contains(100));
        Assert.assertTrue(root.contains(10));
        Assert.assertTrue(root.contains(1000));
        Assert.assertTrue(root.contains(1));

    }

}
